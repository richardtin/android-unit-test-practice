package io.gitlab.richardtin.unittestpractice.holiday;

import java.util.Calendar;

public class Holiday {
    public String sayXmas() {
        Calendar today = Calendar.getInstance();

        if (today.get(Calendar.MONTH) == Calendar.DECEMBER && today.get(Calendar.DAY_OF_MONTH) == 25) {
            return "Merry Xmas";
        }
        return "Today is not Xmas";
    }
}
