package io.gitlab.richardtin.unittestpractice.otp;

import java.util.HashMap;
import java.util.Map;

public class Context {
    private static Map<String, String> profiles;

    static {
        profiles = new HashMap<>();
        profiles.put("joey", "91");
        profiles.put("mary", "99");
    }

    public static String getPassword(String key) {
        return profiles.get(key);
    }
}
