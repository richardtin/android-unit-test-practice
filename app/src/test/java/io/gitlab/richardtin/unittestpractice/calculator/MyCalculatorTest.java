package io.gitlab.richardtin.unittestpractice.calculator;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MyCalculatorTest {

    private MyCalculator calculator;

    @Before
    public void setUp() {
        calculator = new MyCalculator();
    }

    @Test
    public void testPositiveIntegerAddition() {
        sumShouldBe(3, 1, 2);
    }

    private void sumShouldBe(int expected, int first, int second) {
        assertEquals(expected, calculator.add(first, second));
    }
}