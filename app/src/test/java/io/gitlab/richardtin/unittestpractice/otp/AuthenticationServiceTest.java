package io.gitlab.richardtin.unittestpractice.otp;

import org.junit.Test;

import static org.junit.Assert.*;

public class AuthenticationServiceTest {

    public void testIsValid() {
        AuthenticationService target = new AuthenticationService();
        boolean actual = target.isValid("joey", "91000000");
        // always failed
        assertTrue(actual);
    }
}