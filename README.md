# An Android project for unit test practice

## Build

```sh
./gradlew assembleDebug
```

## Test

```sh
./gradlew testDebugUnitTest
```

## Reference

* [Android - Test your app](https://developer.android.com/studio/test/)

## License

This project is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT)
